# Bungee Teleport #

Author: TheeNinja

Bungee Teleport enhances the connections between servers on BungeeCord through signs. A Bungee Teleport sign is created in the following format...

Line 1 - [BungeeTP]
Line 2 - <server>
Line 3 - <comment>
Line 4 - <comment>

A configuration is provided to modify the sent messages for various cases, such as invalid permission or invalid server.


